/**
 * Copyright (c) 2011 - 2017, Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 		Loetz GmbH&Co.KG - Initial implementation
 */
package org.eclipse.osbp.display.api;

import java.util.Map;

import org.eclipse.e4.core.contexts.IEclipseContext;

import com.vaadin.ui.VerticalLayout;

/**
 * Implementations of the interface need to visualize Vaadin pages.<br>
 * Therefore the URL-fragment is passed with the main layout of the UI.<br>
 * {@link IDisplayUiParticipantHandle} are provided by
 * {@link IDisplayUiParticipant} that are related with the fragment.
 */
public interface IDisplayUiParticipantHandle {

	/**
	 * Needs to visualize the site related to the given fragment.
	 *
	 * @param parent the parent
	 * @param context the context
	 */
	void handle(VerticalLayout parent, IEclipseContext context);

	/**
	 * This method will dispose the handle.
	 */
	void dispose();
	
	/**
	 * Send DTO related data to be displayed.
	 *
	 * @param data the data
	 */
	void sendData(Map<String, Object> data);

}
