/**
 * Copyright (c) 2011 - 2017, Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 		Loetz GmbH&Co.KG - Initial implementation
 */
package org.eclipse.osbp.display.api;

import com.vaadin.ui.UI;

/**
 * Implementations of the interface need to provide UI handles. A handle is
 * related with an instance of {@link UI} and an uiId (fragment), and has to
 * visualize mobile UI contents (pages) based on the given URL-fragment.
 * <p>
 * Implementations need to become registered as an OSGi-service. The participant
 * will be found by the OSGi-Property (uriFragment={fragment}).
 */
public interface IDisplayUiParticipant {
	
	public static final String PROPERTY_FRAGMENT_NAME = "uriFragment";

	/**
	 * Creates a new UI handle.
	 *
	 * @param ui the ui
	 * @param fragment the fragment
	 * @return the i display ui participant handle
	 */
	IDisplayUiParticipantHandle createHandle(UI ui, String fragment);
	
	/**
	 * Gets the handle.
	 *
	 * @return the handle
	 */
	IDisplayUiParticipantHandle getHandle();
	
	/**
	 * Sets the handle.
	 */
	void setHandle(IDisplayUiParticipantHandle handle);
	
	/**
	 * Gets the fragment name.
	 *
	 * @return the fragment name
	 */
	public abstract String getFragmentName();

}
